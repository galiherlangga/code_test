@extends('layouts.base')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $title }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ $title }}</h3>
            </div>
            <div class="card-body">
                <form action="{{ $route }}" method="POST" id="candidate-form" enctype="multipart/form-data">
                    @if ($isEdit)
                        @method('PUT')
                    @endif
                    @csrf
                    <input type="hidden" name="id" value="{{@$candidate->id}}">
                    <div class="row">
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" value="{{@$candidate->name}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="education">Education</label>
                                <input type="text" id="education" name="education" class="form-control" value="{{@$candidate->education}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="birthday">Birthday</label>
                                <input type="date" id="birthday" name="birthday" class="form-control" value="{{@$candidate->birthday}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="experience">Experience</label>
                                <input type="number" id="experience" name="experience" class="form-control" value="{{@$candidate->experience}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="last_position">Last Position</label>
                                <input type="text" id="last_position" name="last_position" class="form-control" value="{{@$candidate->last_position}}">
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="applied_position">Applied Position</label>
                                <input type="text" id="applied_position" name="applied_position" class="form-control" value="{{@$candidate->applied_position}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="top_skill">Top 5 Skill</label>
                                <input type="text" id="top_skill" name="top_skill" class="form-control" value="{{@$candidate->top_skill}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" value="{{@$candidate->email}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" id="phone" name="phone" class="form-control" value="{{@$candidate->phone}}" required>
                            </div>
                        </div>
                        <div class=" col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="resume">Resume</label>
                                <input type="file" id="resume" name="resume" class="form-control" accept="application/pdf" {{!$isEdit ? 'required' : ''}}>
                                @if ($isEdit && $candidate)
                                
                                <a href="{{ asset('storage/'.$candidate->resume) }}" class="dark-link" target="_blank">
                                    <div style="width:5rem" class="mt-2">
                                        <img src="{{ asset('images/pdf.png') }}" alt="pdf" srcset="" class="img-thumbnail">
                                    </div>
                                    {{$candidate->resume}}
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer text-center">
                <button type="submit" class="btn btn-primary float-right" form="candidate-form">Submit</button>
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
