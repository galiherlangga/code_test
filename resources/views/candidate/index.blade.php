@extends('layouts.base')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Candidate</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Candidate</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Candidate</h3>
                @can('add')
                <div class="card-tools">
                    <a href="{{ route('candidate.create') }}" class="btn btn-primary">Add Candidate</a>
                </div>
                @endcan
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Education</th>
                            <th>Birthday</th>
                            <th>Email</th>
                            <th>Applied Position</th>
                            @canany(['edit','delete'])
                            <th></th>
                            @endcanany
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

    

    </section>

    @include('components.delete_modal')
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('.datatable').DataTable({
            columns: [
                { data: 'name'},
                { data: 'education'},
                { data: 'birthday'},
                { data: 'email'},
                { data: 'applied_position'},
                @canany(['edit','delete'])
                { data: 'action'},
                @endcanany

            ],
            serverSide: true,
            processing: true,

            ajax: function(data, callback, setting) {
                let token = "{{auth()->user()->createToken('API Token')->accessToken}}";
                $.ajax({
                    method: 'GET',
                    url: '/api/candidate?page='+data.draw,
                    headers: {
                        'Accept': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': "Bearer "+token
                    },
                    data: {
                        limit: data.limit,
                        offset: data.start,
                        search: data.search.value,
                    },
                    success: function(response) {
                        console.log(data)
                        console.log(response)
                        let candidates = new Array();
                        try {
                            response.data.forEach(element => {
                                // console.log(element)
                                let temp = element
                                
                                let url = "{{route('candidate.edit', ':id')}}";
                                url = url.replace(':id', element.id);
                                let editBtn = '';
                                @can('edit', Candidate::class)
                                editBtn = `<a href="${url}" class="btn btn-sm btn-primary candidate-edit-btn" data-id="${element.id}">Edit</a>`;
                                @endcan
                                
                                let deleteBtn = '';
                                @can('delete', Candidate::class)
                                deleteBtn = `<button class="btn btn-danger candidate-delete-btn btn-sm" data-id="${element.id}">Delete</button>`;
                                @endcan

                                temp.action = editBtn + deleteBtn;
                                candidates.push(temp)
                            });
                            console.log(candidates);
                            // console.log(response.data);
                        }catch (error) {
                            console.log(error)
                        }

                        callback({
                            recordsTotal: response.total,
                            recordsFiltered: response.total,
                            data: candidates
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                    }
                })
            },
            
        });
    });

    $(document).on('click', '.candidate-delete-btn', function() {
        $('#modal-delete-message').text("Are you sure you want to delete this candidate?");
        $('#modal-delete-form').attr('action', "{{route('candidate.delete')}}");
        $('#deleted-id').val($(this).data('id'));
        $('#modal-delete').modal('show');
    });
</script>
@endsection