<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Confirmation</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="modal-delete-message"></p>
          <form method="POST" id="modal-delete-form">
            @method('DELETE')
            @csrf
            <input type="hidden" name="id" id="deleted-id">
          </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" form="modal-delete-form">Proceed</button>
        </div>
      </div>
    </div>
  </div>