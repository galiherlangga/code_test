<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.parts.header')
    @yield('style')
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        


<!-- Right navbar links -->
@include('layouts.parts.navbar')

<!-- Main Sidebar Container -->
@include('layouts.parts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

@include('layouts.parts.footer')
@yield('script')
</body>
</html>
