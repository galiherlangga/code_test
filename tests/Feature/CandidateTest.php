<?php

namespace Tests\Feature;

use App\Models\Candidate;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class CandidateTest extends TestCase
{
    /**
     * Access candidate list page.
     *
     * @return void
     */
    public function test_access_candidate_page()
    {
        $user = User::find(1);

        $response = $this->actingAs($user)->get('/candidate');

        $response->assertStatus(200);
    }

    /**
     * Access candidate create page.
     *
     * @return void
     */
    public function test_access_candidate_create_form()
    {
        $user = User::find(1);

        $response = $this->actingAs($user)->get('/candidate/create');

        $response->assertStatus(200);
    }

    /**
     * Create new candidate.
     *
     * @return void
     */
    public function test_create_new_candidate()
    {
        $user = User::find(1);

        $response = $this->actingAs($user)
            ->post('/candidate', [
                'name' => 'John Doe',
                'education' => 'Highschool',
                'birthday' => '1990-01-01',
                'experience' => 5,
                'last_position' => 'Programmer',
                'applied_position' => 'Senior Programmer',
                'top_skill' => 'HTML, CSS, JavaScript, PHP, Golang',
                'email' => 'john.doe@email.com',
                'phone' => '08811223344',
                'resume' => UploadedFile::fake()->create('resume.pdf', 2048)
            ]);
        
        $response->assertRedirect('/candidate');
    }

    /**
     * Access candidate edit page.
     *
     * @return void
     */
    public function test_access_edit_candidate()
    {
        $user = User::find(1);
        $candidate = Candidate::all()->random();

        $response = $this->actingAs($user)->get("/candidate/$candidate->id");

        $response->assertOk();
    }

    /**
     * Update newly created candidate.
     *
     * @return void
     */
    public function test_update_candidate()
    {
        $user = User::find(1);
        $candidate = Candidate::factory()->create();

        $response = $this->actingAs($user)
            ->put('/candidate', [
                'id' => $candidate->id,
                'name' => 'Jane Doe',
                'education' => 'Vocational School',
                'birthday' => '1990-01-01',
                'experience' => 4,
                'last_position' => 'Programmer',
                'applied_position' => 'Senior Programmer',
                'top_skill' => 'HTML, CSS, JavaScript, PHP, Golang',
                'email' => 'john.doe@email.com',
                'phone' => '01122334455',
                'resume' => UploadedFile::fake()->create('resume.pdf', 2048)
            ]);
        
        $response->assertRedirect('/candidate');
    }

    /**
     * Delete newly created candidate.
     *
     * @return void
     */
    public function test_delete_candidate()
    {
        $user = User::find(1);
        $candidate = Candidate::factory()->create();

        $response = $this->actingAs($user)
            ->delete('/candidate', [
                'id' => $candidate->id,
            ]);
        
        $response->assertRedirect('/candidate');
    }
}
