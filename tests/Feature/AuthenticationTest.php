<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    /**
     * Access login page.
     *
     * @return void
     */
    public function test_access_login_page()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    /**
     * Test login with specified user.
     *
     * @return void
     */
    public function test_submit_login()
    {
        $user = User::find(1);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => '12345678'
        ]);
        $this->assertAuthenticated();
        $response->assertRedirect('/');
    }
}
