<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'education',
        'birthday',
        'experience',
        'last_position',
        'applied_position',
        'top_skill',
        'email',
        'phone',
        'resume',
    ];
}
