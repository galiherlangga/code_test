<?php

namespace App\Http\Requests\API;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CandidateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:candidates',
            'name' => 'required|max:255',
            'education' => 'required',
            'birthday' => 'required',
            'experience' => 'required|numeric',
            'last_position' => 'nullable',
            'applied_position' => 'required',
            'top_skill' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'resume' => 'mimes:pdf|max:10000'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validation error',
            'data' => $validator->errors(),
        ]));
    }
}
