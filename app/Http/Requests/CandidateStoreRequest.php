<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CandidateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'education' => 'required',
            'birthday' => 'required',
            'experience' => 'required|numeric',
            'last_position' => 'nullable',
            'applied_position' => 'required',
            'top_skill' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'resume' => 'required|mimes:pdf|max:10000'
        ];
    }
}
