<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\CandidateStoreRequest;
use App\Http\Requests\API\CandidateUpdateRequest;
use App\Traits\CandidateTrait;
use App\Models\Candidate;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    use CandidateTrait;

    /**
     * @OA\Get(
     *      path="/candidate",
     *      operationId="getCandidateList",
     *      tags={"Candidate"},
     *      summary="Get list of candidates",
     *      description="Returns list of candidates",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function list()
    {
        $candidates = Candidate::orderByDesc('id')->paginate();
        return response()->json($candidates);
    }

    /**
     * @OA\Post(
     *      path="/candidate",
     *      operationId="storeCandidate",
     *      tags={"Candidate"},
     *      summary="Store new candidate",
     *      description="Returns candidate data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/CandidateStoreRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Candidate")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(CandidateStoreRequest $request)
    {
        $result = $this->storeCandidate($request);
        return response()->json($result);
    }

    /**
     * @OA\Put(
     *      path="/candidate",
     *      operationId="updateCandidate",
     *      tags={"Candidate"},
     *      summary="Update existing candidate",
     *      description="Returns updated candidate data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/CandidateUpdateRequest")
     *      ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Candidate")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(CandidateUpdateRequest $request)
    {
        $result = $this->updateCandidate($request);
        return response()->json($result);
    }

    /**
     * @OA\Delete(
     *      path="/candidate",
     *      operationId="deleteCandidate",
     *      tags={"Candidate"},
     *      summary="Delete existing candidate",
     *      description="Deletes a record and returns no content",
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function delete(Request $request)
    {
        $result = $this->deleteCandidate($request);
        return response()->json($result);
    }
}
