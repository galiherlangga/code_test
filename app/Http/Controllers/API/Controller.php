<?php

namespace App\Http\Controllers\API;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="CODE Test API Documentation",
 *      description="CODE Test API OpenApi description",
 *      @OA\Contact(
 *          email="galiherlanggadev@gmail.com"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="Demo API Server"
 * )

 *
 * @OA\Tag(
 *     name="Projects",
 *     description="API Endpoints of Projects"
 * )
 */
class Controller
{
}
