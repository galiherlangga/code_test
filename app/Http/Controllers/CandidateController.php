<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Http\Requests\CandidateStoreRequest;
use App\Http\Requests\CandidateUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CandidateController extends Controller
{
    public function index()
    {
        return view('candidate.index');
    }

    public function create()
    {
        $title = "Add New Candidate";
        $isEdit = false;
        $route = route('candidate.store');
        return view('candidate.form', compact('title', 'isEdit', 'route'));
    }

    public function store(CandidateStoreRequest $request)
    {
        $validated = $request->validated();
        
        DB::beginTransaction();
        try {
            $file = Storage::put('public/candidate',$request->file('resume'));
            $validated['resume'] = str_replace("public/","",$file);
            Candidate::create($validated);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            return redirect()->back()->with('warning', "Candidate not saved");
        }
        return redirect()->route('candidate')->with('success', 'Candidate has been saved');
    }

    public function edit($id)
    {
        $candidate = Candidate::find($id);
        $title = "Edit Candidate";
        $isEdit = true;
        $route = route('candidate.update');
        return view('candidate.form', compact('candidate', 'title', 'isEdit', 'route'));
    }

    public function update(CandidateUpdateRequest $request)
    {
        $validated = $request->validated();
        $candidate = Candidate::find($validated['id']);
        DB::beginTransaction();
        try {
            if (isset($validated['resume'])) {
                $file = Storage::put('public/candidate',$request->file('resume'));
                $validated['resume'] = str_replace("public/","",$file);
            }
            $candidate->update($validated);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            return redirect()->back()->with('warning', "Candidate not updated");
        }
        return redirect()->route('candidate')->with('success', 'Candidate has been updated');
    }

    public function delete(Request $request)
    {
        try {
            $candidate = Candidate::findOrFail($request->id);
            $candidate->delete();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            return redirect()->back()->with('warning', "Candidate not deleted");
        }
        return redirect()->route('candidate')->with('success', 'Candidate has been deleted');
    }
}
