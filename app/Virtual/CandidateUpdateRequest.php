<?php

namespace App\Virtual;

/**
 * @OA\Schema(
 *      title="Update Candidate request",
 *      description="Update Candidate request body data",
 *      type="object",
 *      required={"name", "education", "birthday", "experience", "applied_position", "top_skill", "email", "phone", "resume"}
 * )
 */
class CandidateUpdateRequest
{
    /**
     * @OA\Property(
     *     title="id",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *      title="name",
     *      description="Name of the candidate",
     *      example="John Doe"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="education",
     *      description="Education of the candidate",
     *      example="Highschool"
     * )
     *
     * @var string
     */
    public $education;

    /**
     * @OA\Property(
     *      title="birthday",
     *      description="Birthday of the candidate",
     *      example="1990-01-01"
     * )
     *
     * @var string
     */
    public $birthday;


    /**
     * @OA\Property(
     *      title="experience",
     *      description="Experience of the candidate in years",
     *      example="5"
     * )
     *
     * @var integer
     */
    public $experience;

    /**
     * @OA\Property(
     *      title="last_position",
     *      description="Last position of the candidate",
     *      example="Programmer"
     * )
     *
     * @var string
     */
    public $last_position;

    /**
     * @OA\Property(
     *      title="applied_position",
     *      description="Applied Position of the candidate",
     *      example="Senior Programmer"
     * )
     *
     * @var string
     */
    public $applied_position;

    /**
     * @OA\Property(
     *      title="top_skill",
     *      description="Top 5 skill of the candidate",
     *      example="HTML, CSS, JavaScript, PHP, Node"
     * )
     *
     * @var string
     */
    public $top_skill;

    /**
     * @OA\Property(
     *      title="email",
     *      description="Email of the candidate",
     *      example="john.doe@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="phone",
     *      description="Phone of the candidate",
     *      example="088111222333"
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *      title="resume",
     *      description="Resume of the candidate in pdf",
     *      example="resume.pdf"
     * )
     *
     * @var string
     */
    public $resume;

}