<?php

namespace App\Virtual\Models;

/**
 * @OA\Schema(
 *     title="Candidate",
 *     description="Candidate model",
 *     @OA\Xml(
 *         name="Candidate"
 *     )
 * )
 */
class Candidate
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *      title="Name",
     *      description="Name of the candidate",
     *      example="John Doe"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *      title="Education",
     *      description="Education of the candidate",
     *      example="Highschool"
     * )
     *
     * @var string
     */
    public $education;

    /**
     * @OA\Property(
     *      title="Birthday",
     *      description="Birthday of the candidate",
     *      example="1990-01-01"
     * )
     *
     * @var \Date
     */
    public $birthday;


    /**
     * @OA\Property(
     *      title="Experience",
     *      description="Experience of the candidate in years",
     *      example="5"
     * )
     *
     * @var integer
     */
    public $experience;

    /**
     * @OA\Property(
     *      title="Last Position",
     *      description="Last position of the candidate",
     *      example="Programmer"
     * )
     *
     * @var string
     */
    public $last_position;

    /**
     * @OA\Property(
     *      title="Applied Position",
     *      description="Applied Position of the candidate",
     *      example="Senior Programmer"
     * )
     *
     * @var string
     */
    public $applied_position;

    /**
     * @OA\Property(
     *      title="Top 5 Skill",
     *      description="Top 5 skill of the candidate",
     *      example="HTML, CSS, JavaScript, PHP, Node"
     * )
     *
     * @var string
     */
    public $top_skill;

    /**
     * @OA\Property(
     *      title="Email",
     *      description="Email of the candidate",
     *      example="john.doe@email.com"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *      title="Phone",
     *      description="Phone of the candidate",
     *      example="088111222333"
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *      title="Resume",
     *      description="Resume of the candidate in pdf",
     *      example="resume.pdf"
     * )
     *
     * @var string
     */
    public $resume;
}
