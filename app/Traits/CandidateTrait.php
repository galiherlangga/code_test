<?php

namespace App\Traits;

use App\Models\Candidate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait CandidateTrait
{
    public function storeCandidate($request)
    {
        $validated = $request->validated();
        DB::beginTransaction();
        try {
            $file = Storage::put('public/candidate',$request->file('resume'));
            $validated['resume'] = str_replace("public/","",$file);
            $candidate = Candidate::create($validated);
            DB::commit();
            return ['success' => true, "data" => $candidate];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            return ['success' => false, "data" => $th];
        }
    }

    public function updateCandidate($request)
    {
        $validated = $request->validated();
        $candidate = Candidate::find($validated['id']);
        DB::beginTransaction();
        try {
            if (isset($validated['resume'])) {
                $file = Storage::put('public/candidate',$request->file('resume'));
                $validated['resume'] = str_replace("public/","",$file);
            }
            $candidate->update($validated);
            DB::commit();
            return ['success' => true, "data" => $candidate];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            return ['success' => false, "data" => $th];
        }
    }

    public function deleteCandidate($request)
    {
        try {
            $candidate = Candidate::findOrFail($request->id);
            $candidate->delete();
            return ['success' => true, "data" => $candidate];
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            return ['success' => false, "data" => $th];
        }
    }
}