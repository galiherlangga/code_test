<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CandidateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'education' => $this->faker->randomElement(['High School', 'Vocational School', 'University']),
            'birthday' => $this->faker->dateTimeBetween('-30 years', '-20 years'),
            'experience' => $this->faker->numberBetween(1, 10),
            'last_position' => $this->faker->jobTitle(),
            'applied_position' => $this->faker->jobTitle(),
            'top_skill' => "Laravel, Mysql, PostgreSQL, Codeigniter, Java",
            'email' => $this->faker->email(),
            'phone' => $this->faker->phoneNumber(),
            'resume' => 'sample.pdf',
        ];
    }
}
