<?php

namespace Database\Seeders;

use App\Models\Candidate;
use Illuminate\Database\Seeder;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Candidate::factory()->count(99)->create();
        Candidate::create([
            'name' => 'Smith',
            'education' => 'UGM Yogyakarta',
            'birthday' => '1991-01-19',
            'experience' => 5,
            'last_position' => 'CEO',
            'applied_position' => 'Senior PHP Devloper',
            'top_skill' => 'Laravel, Mysql, PostgreSQL, Codeigniter, Java',
            'email' => 'smith@gmail.com',
            'phone' => '085123456789',
            'resume' => 'sample.pdf'
        ]);

    }
}
