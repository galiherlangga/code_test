<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;


class PermissionDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'view']);
        Permission::create(['name' => 'read']);
        Permission::create(['name' => 'edit']);
        Permission::create(['name' => 'add']);
        

        $seniorRole = Role::create(['name' => 'Senior HRD']);
        $seniorRole->givePermissionTo('view');
        $seniorRole->givePermissionTo('read');
        $seniorRole->givePermissionTo('edit');
        $seniorRole->givePermissionTo('add');

        $hrdRole = Role::create(['name' => 'HRD']);
        $hrdRole->givePermissionTo('view');

        $superAdminRole = Role::create(['name' => 'super-admin']);

        $user = User::factory()->create([
            'name' => 'Mr. John',
            'email' => 'john@email.com',
            'password' => bcrypt('12345678')
        ]);
        $user->assignRole($seniorRole);

        $user = User::factory()->create([
            'name' => 'Mrs. Lee',
            'email' => 'lee@email.com',
            'password' => bcrypt('12345678')
        ]);
        $user->assignRole($hrdRole);
        
        $user = User::factory()->create([
            'name' => 'superadmin',
            'email' => 'superadmin@email.com',
            'password' => bcrypt('12345678')
        ]);
        $user->assignRole($superAdminRole);
    }
}
