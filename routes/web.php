<?php


use App\Http\Controllers\HomeController;
use App\Http\Controllers\CandidateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('', [HomeController::class, 'index'])->name('dashboard');
Route::prefix('candidate')->group(function() {
    Route::get('', [CandidateController::class, 'index'])->name('candidate');
    Route::get('create', [CandidateController::class, 'create'])->name('candidate.create');
    Route::post('', [CandidateController::class, 'store'])->name('candidate.store');
    Route::get('{id}', [CandidateController::class, 'edit'])->name('candidate.edit');
    Route::put('', [CandidateController::class, 'update'])->name('candidate.update');
    Route::delete('', [CandidateController::class, 'delete'])->name('candidate.delete');
});
