<?php

use App\Http\Controllers\API\UserAuthController;
use App\Http\Controllers\API\CandidateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', [UserAuthController::class, 'login'])->name('api.auth.login');
Route::post('register', [UserAuthController::class, 'register'])->name('api.auth.register');

Route::group(['middleware' => ['cors','auth:api']], function() {
    Route::prefix('candidate')->group(function() {
        Route::get('', [CandidateController::class, 'list']);
        Route::post('', [CandidateController::class, 'store']);
        Route::put('', [CandidateController::class, 'update']);
        Route::delete('', [CandidateController::class, 'delete']);
    });
    
});
